"use strict";
jQuery(document).ready(function ($) {

    $(window).load(function () {
        $(".loaded").fadeOut();
        $(".preloader").delay(1000).fadeOut("slow");
    });
    /*---------------------------------------------*
     * Mobile menu
     ---------------------------------------------*/
    $('#bs-example-navbar-collapse-1').find('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 0)
                }, 1000);
                if ($('.navbar-toggle').css('display') != 'none') {
                    $(this).parents('.container').find(".navbar-toggle").trigger("click");
                }
                return false;
            }
        }
    });


    /*---------------------------------------------*
     * WOW
     ---------------------------------------------*/

    var wow = new WOW({
        mobile: false // trigger animations on mobile devices (default is true)
    });
    wow.init();



    /* ---------------------------------------------------------------------
     Carousel
     ---------------------------------------------------------------------= */

    $('.main_home_slider').owlCarousel({
        responsiveClass: true,
        autoplay: false,
        items: 1,
        loop: true,
        dots: true,
        nav: false,
        navText: [
            "<i class='lnr lnr-chevron-left'></i>",
            "<i class='lnr lnr-chevron-right'></i>"
        ],
        autoplayHoverPause: true

    });

    $('.single_features_slide').owlCarousel({
        responsiveClass: true,
        autoplay: false,
        items: 1,
        loop: true,
        dots: true,
        nav: false,
        navText: [
            "<i class='lnr lnr-chevron-left'></i>",
            "<i class='lnr lnr-chevron-right'></i>"
        ],
        autoplayHoverPause: true

    });

    $('.main_teastimonial_slider').owlCarousel({
        responsiveClass: true,
        autoplay: false,
        items: 1,
        loop: true,
        dots: true,
        nav: false,
        navText: [
            "<i class='lnr lnr-chevron-left'></i>",
            "<i class='lnr lnr-chevron-right'></i>"
        ],
        autoplayHoverPause: true

    });


// magnificPopup

    $('.portfolio-img').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

//mytabs

//    $('ul.service_tabe_menu li a').click(function (e) {
//        e.preventDefault()
//        $(this).tab('show')
//    });

//skillbar section

    // var skillBarTopPos = jQuery('.skillbar').position().top;
    // jQuery(document).scroll(function () {
    //     var scrolled_val = $(document).scrollTop().valueOf();
    //     if (scrolled_val > skillBarTopPos - 250)
    //         startAnimation();
    // });

    function startAnimation() {
        jQuery('.skillbar').each(function () {
            jQuery(this).find('.skillbar-bar').animate({
                width: jQuery(this).attr('data-percent')
            }, 6000);
        });
    }
    ;


//---------------------------------------------
// Counter 
//---------------------------------------------

    $('.statistic-counter').counterUp({
        delay: 10,
        time: 2000
    });

// main-menu-scroll

    jQuery(window).scroll(function () {
        var top = jQuery(document).scrollTop();
        var height = 300;
        //alert(batas);

        if (top > height) {
            jQuery('.navbar-fixed-top').addClass('menu-scroll');
        } else {
            jQuery('.navbar-fixed-top').removeClass('menu-scroll');
        }
    });

// scroll Up

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('.scrollup').fadeIn('slow');
        } else {
            $('.scrollup').fadeOut('slow');
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
        return false;
    });

//    $('#menu').slicknav();
    jQuery('#portfoliowork').mixItUp({
        selectors: {
            target: '.tile',
            filter: '.filter',
            sort: '.sort-btn'
        },
        animation: {
            animateResizeContainer: false,
            effects: 'fade scale'
        }

    });



    $('.dropdown-menu').click(function (e) {
        e.stopPropagation();
    });

    //End

});



$(document).on("scroll", function () {
    if ($(document).scrollTop() > 120) {
        $("nav").addClass("small");
        $("#main_menu").addClass("main_menu_bg_scroll");
    } else {
        $("nav").removeClass("small");
        $("#main_menu").removeClass("main_menu_bg_scroll");
    }
});


$(document).on('click', '#form-button', function(event) {
    //debugger;
    event.preventDefault();
    $('.loading_image').removeClass('hidden');
    $('#form-button').addClass('hidden');

    var url = baseurl + "Logic/contact_form";
    $.ajax({
        url: url,
        method: "POST",
        dataType: "JSON",
        data: $('#formid').serialize(),
        success: function (resp)
        {
            if (resp.msg == "fail") 
            {

                if( resp.name.length !== 0) {
                    $('.name_input').addClass('error_input');
                }
                else {
                    $('.name_input').removeClass('error_input');
                }

                if( resp.surname.length !== 0) {
                    $('.surname_input').addClass('error_input');
                }
                else {
                    $('.surname_input').removeClass('error_input');
                }
                if( resp.cellphone.length !== 0) {
                    $('.cellphone_input').addClass('error_input');
                }
                else {
                    $('.cellphone_input').removeClass('error_input');
                }

                if( resp.email.length !== 0) {
                    $('.email_input').addClass('error_input');
                }
                else {
                    $('.email_input').removeClass('error_input');
                }

                if( resp.message.length !== 0) {
                    $('.message_input').addClass('error_input');
                }
                else {
                    $('.message_input').removeClass('error_input');
                }

                //$(".message-loading").css('display', 'none');
                $('#form-button').removeClass('hidden');
                $('.loading_image').addClass('hidden');
                
            }
            else 
            {

                if (resp.msg == "Message Sent" ) 
                {
                    $('.sent_message').removeClass('hidden');
                    $('.loading_image').addClass('hidden');
                }
                
                if (resp.msg == "Message Failed") 
                {

                    $('#form-button').removeClass('hidden');
                    $('.loading_image').addClass('hidden');

                }
            }
            


        }
    });
    
});



