<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logic extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //$this->load->library('curl'); 
        //$this->load->library('session');
    }

	public function index()
	{
		$data = array('page' => 'main_v');
		$this->load->view('template', $data);
	}

	function contact_form()
	{
		//printf("Hello World");
		//$this->load->library('recaptcha');

		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('surname','Surname','required');
        $this->form_validation->set_rules('cellphone','cellphone','required|numeric|min_length[9]|max_length[10]');
        $this->form_validation->set_rules('message','message','required');
        $this->form_validation->set_rules('email','email','required|valid_email');

        $captcha_answer = $this->input->post('g-recaptcha-response');

        $response = $this->recaptcha->verifyResponse($captcha_answer);

        if ($response['success']) {

            if ($this->form_validation->run() == TRUE)
            {
                $data =  array( 'name'      => $this->input->post('name'),
                                'surname'   => $this->input->post('surname'),
                                'cellphone' => $this->input->post('cellphone'),
                                'message'   => $this->input->post('message'),
                                'email'     => $this->input->post('email')
                                );

                $this->send_email($data);
            }
            else
            {
                $data = array(
                        'name'          => form_error ('name'),
                        'surname'       => form_error ('surname'),
                        'cellphone'     => form_error ('cellphone'),
                        'email'         => form_error ('email'),
                        'message'       => form_error ('message'),
                        'msg'           => "fail"
                    );

                    echo json_encode($data);

            }
        } else {
            // Something goes wrong
            //echo "<pre>";print_r("Working");echo "</pre>";die();
            var_dump($response);
        }


        
		


	}

	function send_email($data)
    {

        $this->email->to("info@vrcattorneys.co.za");
        $this->email->subject("Message from VRCATTORNEYS Website");

        $this->email->message($data['name']."".$data['surname']."".$data['message']."".$data['cellphone']);
        $r = $this->email->send();
        sleep(5);
        if($r == TRUE) {

        	$data['msg']	=	"Message Sent";
            echo json_encode($data);

        }
        else {
            $data['msg'] = "Message Failed";
            echo json_encode($data);

        }

    }
}
