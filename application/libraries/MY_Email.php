<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Email Class
 *
 * Permits email to be sent using Mail, Sendmail, or SMTP.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Libraries
 * @author      Paulo Caldeira <paulo.caldeira@acin.pt>
 */
class MY_Email extends CI_Email {
    /**
     * Custom configurations
     * @var array
     */
    protected $customConfigs = array();

    /**
     * MY Email constructor
     * @param array $config Email configurations
     */
    function __construct(array $config = array()) {
        $ci =& get_instance();
        $ci->config->load('email', true);

        $emailConfig = $ci->config->item('email') ?: array();
        $this->customConfigs = array_merge(array(), $emailConfig, $config);

        parent::__construct($this->customConfigs);

        // Set default from
        parent::from($this->customConfigs['from'], $this->customConfigs['from_name']);
    }

    /**
     * Set from email
     * <b>Note:</b> This method will prevent from changing from email because it could be dangerous
     *
     *                                      !DON'T DELETE IT!
     *
     * @param  string   $email       From email
     * @param  string   $name        From name
     * @param  string   $return_path Optional email address to redirect undelivered e-mail to
     * @return MY_Email              Email class
     */
    public function from($email, $name = '', $return_path = NULL)
    {
        if (empty($email) ||
            empty($this->customConfigs['from']) ||
            $email != $this->customConfigs['from']) {

            throw new Exception("Please check your `from` email because it's empty or it difers from your " .
                "configured one in config/email.php. Be careful when changing it.");
        }

        return parent::from($email, $name, $return_path);
    }

    /**
     * Set Recipients
     *
     * @param   string
     * @return  CI_Email
     */
    public function to($to)
    {
        // Check if it's using a production account in development
        if (defined('ENVIRONMENT') && ENVIRONMENT != 'production' && preg_match('/\@vrcatttorneys\.co.za/i', $to)) {
            throw new Exception("You're using a production email (" . $to . "). Please check your config/email.php and replace it.", 1);
        }

        return parent::to($to);
    }
}