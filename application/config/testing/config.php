<?php

/**
 * Location of uploaded files
 */
$config['upload_dir'] = realpath(FCPATH . '../espaco_upload');

/**
 * Company's information email
 * Please note that emails possibly could be sent to this email when you're changing it.
 * Note: Use it carefully because it can be dangerous
 */
$config['company_email'] = '[dev_email]';