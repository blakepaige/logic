<?php
/**
 * Email Configurations
 */
$config['protocol']   = 'smtp';
$config['smtp_host']  = 'mail.vrcattorneys.co.za';
$config['smtp_port']  = '587';
$config['smtp_user']  = 'info@vrcattorneys.co.za';
$config['smtp_pass']  = 'admin.??12345';
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype']   = 'html';
$config['validation'] = TRUE;

/**
 * Please note that emails possibly could be sent from/to this email when you're changing it.
 * Note: Use it carefully because it can be dangerous
 */
$config['from'] = 'vrcattorneys website';
$config['from_name'] = 'Vusi Rajuili Commercial and Labour Law Firm';