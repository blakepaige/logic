<form id="formid" method="post" >

    <!--<div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">-->
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="name_input form-control" name="name" placeholder="Name" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="surname_input form-control" name="surname" placeholder="Surname" required>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="email_input form-control" name="email" placeholder="Email" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="cellphone_input form-control" name="cellphone" placeholder="Cellphone" required>
            </div>
        </div>
    </div>


    <div class="form-group">
        <textarea class="message_input form-control" name="message" rows="8" placeholder="Message"></textarea>
    </div>

    <!-- <div class="g-recaptcha" data-sitekey="6Lf5nEwUAAAAAFy1_rEM2dgn_jlbxBikYK5zva5a"></div> -->
    <?php echo $this->recaptcha->render(); ?>
    
    <img class="loading_image hidden" src="<?= base_url("assets/images/Spin-1s-200px.gif");?>" alt="">
    <div class="">
        <input type="submit " value="Submit" class="btn btn-primary submit_button " id="form-button">
        <h3 class="sent_message hidden">Message Sent <i class="fas fa-check"></i></h3>
        <h3 class="error_sent_message hidden">Message Not Sent <i class="fas fa-times"></i></h3>
    </div>

</form>