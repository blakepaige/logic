<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

            <section id="home" class="home">
                <div class="overlay">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class=" text-center">
                                    <div class="single_home_slider">
                                        <div class="main_home wow fadeInUp" data-wow-duration="700ms">
                                            <h1>Welcome to Vusi Rajuili's Law Firm</h1>
                                            <p>WE WALK WITH YOU.</p>
                                            <div class="home_btn">
                                                <a href="#about-us" class="btn btn-primary">LEARN MORE</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<div class="container">
	<div class="row">
		<div class="contain-about-us" id="about-us">
			<div class="about-us-section text-center">
				<h2>
					About Us
				</h2>
				<div class="separator about-us-seperator"></div>
				<div class="col-12"></div>
				<p class="about-us-intro">
					The group was founded in response to an identified need of law firms to strategically position themselves for the future and access the economies of scale of a national group with national visibility and brand awareness.
				</p>
			</div>
		</div>
		<!-- The First Thingggy -->
		<div class="col-md-4">
			<div class="mission">
				<i class="fa fa-eye"></i>
				<h3>Vision</h3>
				<p class="about-us-info">Our vision is to provide our clients with skilled legal advice in a timely and efficient manner.</p>
				<a href="#">Read more</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="mission">
				<i class="fa fa-bullseye"></i>
				<h3>Mission</h3>
				<p class="about-us-info">The firm’s mission is to provide a high quality, creative, and result – oriented legal team to individuals and businesses.</p>
				<a href="#">Read more</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="mission">
				<i class="fa fa-balance-scale"></i>
				<h3>Values</h3>
				<p class="about-us-info">INTEGRITY – We lead by example in all we do.  We set the highest goals of honesty and ethics.</p>
				<a href="#">Read more</a>
			</div>
		</div>
	</div>
</div>

<section id="counter" class="counter">
    <div class="video_overlay">
        <div class="container">
            <div class="row">  
                <div class="col-sm-12">               
                    <div class="main_counter_area text-center">
						<div class="banner-button">
							<h2>A Broader Perspective.</h2>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!-- End of counter section -->
<div class="practice-area-section text-center" id="practice-area">
	<h2>
		Practice Area
	</h2>
	<div class="separator practice-area-seperator"></div>
</div>

<div class="container">
	<div class="row">

		<!-- The First Thingggy -->
		<div class="col-md-4">
			<div class="mission">
				<i class="fa fa-bar-chart"></i>
				<h3>Commercial Law</h3>
				<p class="practice-area-info">Commercial law disputes deal primarily with contract and/or tort laws. It involves issues that arise in the course of running a business at any stage of the commercial cycle.</p>
				<a href="#">Read more</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="mission">
				<i class="fas fa-paperclip"></i>
				<h3>Labour Law</h3>
				<p class="practice-area-info">the relationship between workers, employing entities, trade unions and the government. Collective labour law relates to the tripartite relationship between employee, employer and union.</p>
				<a href="#">Read more</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="mission">
				<i class="fas fa-bullhorn"></i>
				<h3>Public Law</h3>
				<p class="practice-area-info">part of law which governs relationships between individuals and the government, and those relationships between individuals which are of direct concern to society.</p>
				<a href="#">Read more</a>
			</div>
		</div>
	</div>
</div>

<section id="choose" class="choose">
    <div class="container">
        <div class="row">
            <div class="main_choose sections">
                <div class="col-sm-6">
                    <div class="head_title">
                        <h3>WHY CHOOSE US</h3>
                        <div class="separator"></div>
                    </div>
                    <div class="single_choose">
                        <div class="single_choose_acording">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                <i class="fa fa-picture-o"></i> Prestige.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: auto;">
                                        <div class="panel-body">
                                            With prestige generally comes more sophisticated and more complex work. But another reason the prestige of your first firm is so important is simply that your next prospective employers will see it as important.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-map-signs"></i> Striking the right balance.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                                        <div class="panel-body">
                                            The team at VRC is a happy blend of generalist-specialist commercial solicitors. We are lawyers with experience of running our own businesses: commercial and entrepreneurial business people, but with legal training and qualifications.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="fa fa-bullseye"></i> Not afraid to say “no”.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                        <div class="panel-body">
                                            Truelegal’s success is underpinned by our commitment to doing the right type of business, not by winning just any business. We know what we’re good at and we enjoy making a difference for our clients.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="fa fa-umbrella"></i>Good chemistry.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                        <div class="panel-body">
                                            When choosing a solicitor, don’t underestimate personality. Choose a solicitor whom you like, trust and can relate to. Choose a solicitor who talks your language, shows a genuine interest and who makes themselves available when you need them.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="fa fa-subway"></i>Make a free enquiry.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive" aria-expanded="false">
                                        <div class="panel-body">
                                            The sooner we become involved in the process of helping you to buy or sell your business, the better understanding you will have of the associated risk and the more likely it is that your transaction will be successful.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="single_choose">
                        <div class="single_choose_img">
                            <img src="assets/images/olu-eletu-28020.jpg" alt="">                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="testimonial" class="testimonial">
    <div class="video_overlay">
        <div class="container">
            <div class="row">
                <div class="main_testimonial sections text-center">
                    <div class="col-md-12" data-wow-delay="0.2s">
                        <div class="main_teastimonial_slider text-center">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="contact" class="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="contact_contant sections">
                        <div class="col-sm-6">

                            <div class="main_contact_info">
                                <div class="head_title">
                                    <h3>CONTACT INFO</h3>
                                    <div class="separator"></div>
                                </div>
                                <div class="row">
                                    <div class="contact_info_content">
                                        <div class="col-sm-12">
                                            <div class="single_contact_info">
                                                <div class="single_info_icon">
                                                    <i class="fa fa-home"></i>
                                                </div>
                                                <div class="single_info_text">
                                                    <h3>VISIT US</h3>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="single_contact_info">
                                                <div class="single_info_icon">
                                                    <i class="fa fa-envelope-o"></i>
                                                </div>
                                                <div class="single_info_text">
                                                    <h3>MAIL US</h3>
                                                    <p>info@vrcattorneys.co.za</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="single_contact_info">
                                                <div class="single_info_icon">
                                                    <i class="fa fa-mobile"></i>
                                                </div>
                                                <div class="single_info_text">
                                                    <h3>CALL US</h3>
                                                    <p>0797525728</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="single_contact_info">
                                                <div class="single_info_icon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                                <div class="single_info_text">
                                                    <h3>WORK HOUR</h3>
                                                    <p>Mon - Sat: 08 Am - 17 Pm</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="col-sm-6">
                            <div class="head_title">
                                <h3>LEAVE MESSAGE</h3>
                                <div class="separator"></div>
                            </div>
                            <div class="single_contant_left">
                                <?php $this->load->view('pages/message_form_v'); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>  <!-- End of contact section -->



<section id="maps" class="maps">
    <div class="map-overlay">
        <div class="container-fluid">
            <div class="row">
                <div class="main_maps text-center">
                    <div class="col-sm-12 no-padding">
                        <div class="map_canvas_icon">
                            <i class="fa fa-map-marker" onClick="showmap()"></i>
                            <h2 onClick="showmap()">FIND US ON GOOGLE MAP</h2>
                        </div>
                        <div id="map_canvas"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="footer" class="footer_widget">
    <div class="video_overlay">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="main_widget">
                            <div class="col-sm-3 col-xs-12">
                                <div class="single_widget wow fadeIn" data-wow-duration="800ms">
                                    <div class="footer_logo">
                                        <!-- <img src="assets/images/logo.png" alt="" /> -->
                                    </div>
                                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                                        nisi ut aliquip ex ea commodo consequat. </p> -->

                                </div>
                            </div>

                            <div class="col-sm-3  col-xs-12">
                                <div class="single_widget wow fadeIn" data-wow-duration="800ms">

                                   <!--  <div class="footer_title">
                                        <h4>SITEMAP</h4>
                                        <div class="separator"></div>
                                    </div>
                                    <ul>
                                        <li><a href="">Services</a></li>
                                        <li><a href="">About Us</a></li>
                                        <li><a href="">Our Team</a></li>
                                        <li><a href="">Portfolio</a></li>
                                    </ul>  -->
                                </div>
                            </div>

                            <div class="col-sm-3  col-xs-12">
                                <div class="single_widget wow fadeIn" data-wow-duration="800ms">

                                    <!-- <div class="footer_title">
                                        <h4>ACHIVES</h4>
                                        <div class="separator"></div>
                                    </div>
                                    <ul>
                                        <li><a href="">January 2015</a></li>
                                        <li><a href="">February 2015</a></li>
                                        <li><a href="">March 2015</a></li>
                                        <li><a href="">April 2015</a></li>
                                    </ul>  -->
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="single_widget wow fadeIn" data-wow-duration="800ms">

                                    <div class="footer_title">
                                        <h4>NEWSLETTER</h4>
                                        <div class="separator"></div>
                                    </div>

                                    <div class="footer_subcribs_area">
                                        <p>Sign up for our mailing list to get latest updates and offers.</p>
                                        <form class="navbar-form navbar-left" role="search">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Email">
                                                <button type="submit" class="submit_btn"></button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>