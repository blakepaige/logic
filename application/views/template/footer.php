<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
		


		<section class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main_footer">
                                <div class="row">

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="copyright_text">
                                            <p class=" wow fadeInRight" data-wow-duration="1s">2018. All Rights Reserved</p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="flowus">
                                            <a href="https://www.facebook.com/Vusi-Rajuili-Commercial-and-Labour-Law-Attorneys-354460088275238/"><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <!-- <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-instagram"></i></a>
                                            <a href=""><i class="fa fa-youtube"></i></a> -->
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
       </div>

        <!-- START SCROLL TO TOP  -->

        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>

        <script src="<?= base_url("assets/js/vendor/jquery-1.11.2.min.js");?>"></script>
        <script src="<?= base_url("assets/js/vendor/bootstrap.min.js");?>"></script>

        <script src="<?= base_url("assets/js/jquery.magnific-popup.js");?>"></script>
        <script src="<?= base_url("assets/js/jquery.mixitup.min.js");?>"></script>
        <script src="<?= base_url("assets/js/jquery.easing.1.3.js");?>"></script>
        <script src="<?= base_url("assets/js/jquery.masonry.min.js");?>"></script>

        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        <script src="http://maps.google.com/maps/api/js"></script>
        <script src="assets/js/gmaps.min.js"></script>


        <script>

                                            function showmap() {
                                                var mapOptions = {
                                                    zoom: 8,
                                                    scrollwheel: false,
                                                    center: new google.maps.LatLng(-34.397, 150.644),
                                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                                };
                                                var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                                            }
        </script> -->

        <script src="<?= base_url("assets/js/plugins.js");?>"></script>
        <script src="<?= base_url("assets/js/main.js");?>"></script>

    </body>
</html>