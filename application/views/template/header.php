<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang="eng"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Vusi Rajuili's Law Firm</title>
        <meta name="description" content="Vusi Rajuili's Commercial and Public Law Firm">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= base_url("assets/images/favicon.ico");?>" type="image/x-icon">

        <!--Google Font link-->
       <!--  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet"> -->

        <link rel="stylesheet" href="<?= base_url("assets/css/web-fonts-with-css/css/fontawesome-all.min.css");?>">
        <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.min.css");?>">
        <link rel="stylesheet" href="<?= base_url("assets/css/magnific-popup.css");?>">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="<?= base_url("assets/css/plugins.css");?>" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="<?= base_url("assets/css/style.css");?>">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="<?= base_url("assets/css/responsive.css");?>" />

        <script src="<?= base_url("assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js");?>"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script>
        	var baseurl = "<?= base_url();?>";
        	
        </script>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="200">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class='preloader'>
            <div class='loaded'>&nbsp;</div>
        </div>
        <div class="culmn">
